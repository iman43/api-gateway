package routes

import (
	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
)

// SwaggerRoute func for describe group of API Docs routes.
func SwaggerRoute(r *mux.Router) {
	// Create routes group.
	route := r.PathPrefix("/swagger")

	// Routes for GET method:
	route.Handler(httpSwagger.WrapHandler).Methods("GET") // get one user by ID
}
