package routes

import (
	"github.com/gorilla/mux"
	"github.com/doston/iman/api_gateway/api/controllers"
)

// UserRoutes func for describe group of public routes.
func FetchRoutes(r *mux.Router) {
	// Create routes group.
	route := r.PathPrefix("/api/v1").Subrouter()

	route.HandleFunc("/post/fetch-posts/", controllers.FetchPosts).Methods("POST") // create a post
}
