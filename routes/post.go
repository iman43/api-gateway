package routes

import (
	"github.com/gorilla/mux"
	"github.com/doston/iman/api_gateway/api/controllers"
)

// UserRoutes func for describe group of public routes.
func PostRoutes(r *mux.Router) {
	// Create routes group.
	route := r.PathPrefix("/api/v1").Subrouter()

	// // // Routes for POST method:
	route.HandleFunc("/post/create-post/", controllers.CreatePost).Methods("POST") // create a post
	route.HandleFunc("/post/get-post/{post_id}/", controllers.GetPostWithId).Methods("GET") // get a certain post
	route.HandleFunc("/post/list-posts/", controllers.GetPosts).Methods("GET") // get posts
	route.HandleFunc("/post/update-post/", controllers.UpdatePost).Methods("PUT") // update post
	route.HandleFunc("/post/delete-post/{post_id}/", controllers.DeletePost).Methods("DELETE") // delete post

	// route.HandleFunc("/user/login/", controllers.Login).Methods("POST")           // auth, return Access & Refresh tokens
	
}
