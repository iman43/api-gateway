package controllers

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	//ErrorCodeInvalidJSON ...
	ErrorCodeInvalidJSON = "INVALID_JSON"
	//ErrorCodeInvalidParams ...
	ErrorCodeInvalidParams = "INVALID_PARAMS"
	//ErrorCodeInternal ...
	ErrorCodeInternal = "INTERNAL"
	//ErrorCodeAlreadyExists ...
	ErrorCodeAlreadyExists = "ALREADY_EXISTS"
)

func handleGrpcErrWithMessage(w http.ResponseWriter, err error, args ...interface{}) error {
	if err == nil {
		return nil
	}
	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, response{Error: true,
			Data: Error{
				Status:  http.StatusInternalServerError,
				Message: st.Message(),
			}})
		return err

	} else if st.Code() == codes.NotFound {
		fmt.Println(err)
		w.WriteHeader(http.StatusNotFound)
		writeJSON(w, response{Error: true,
			Data: Error{
				Status:  http.StatusNotFound,
				Message: st.Message(),
			}})
		return err

	} else if st.Code() == codes.AlreadyExists {
		fmt.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, response{Error: true,
			Data: Error{
				Status:  http.StatusBadRequest,
				Message: st.Message(),
			}})
		return err

	}
	fmt.Println(err)
	w.WriteHeader(http.StatusInternalServerError)
	writeJSON(w, response{Error: true,
		Data: Error{
			Status:  http.StatusInternalServerError,
			Message: st.Message(),
		}})

	return err
}

// Error ...
type Error struct {
	Status  int
	Message string
}

func handleInternalWithMessage(w http.ResponseWriter, err error, message string) error {
	if err == nil {
		return nil
	}

	log.Panicln(message+" ", err)
	w.WriteHeader(http.StatusInternalServerError)
	writeJSON(w, response{Error: true,
		Data: Error{
			Status:  http.StatusInternalServerError,
			Message: message,
		}})
	return err
}

func handleBadRequestErrWithMessage(w http.ResponseWriter, err error, message string) error {
	if err == nil {
		return nil
	}

	log.Println(message+" ", err)
	w.WriteHeader(http.StatusBadRequest)
	writeJSON(w, response{Error: true,
		Data: Error{
			Status:  http.StatusBadRequest,
			Message: message + err.Error(),
		}})
	return err
}


// ParsePageQueryParam ...
func ParsePageQueryParam(r *http.Request) (int, error) {
	pageparam := r.URL.Query().Get("page")
	if pageparam == "" {
		return 1, nil
	}

	page, err := strconv.Atoi(pageparam)
	if err != nil {
		return 0, err
	}
	if page < 0 {
		return 0, errors.New("page must be an positive integer")
	}
	if page == 0 {
		return 1, nil
	}
	return page, nil
}

// ParseLimitQueryParam ...
func ParseLimitQueryParam(r *http.Request) (int, error) {

	limitparam := r.URL.Query().Get("limit")
	if limitparam == "" {
		return 10, nil
	}

	page, err := strconv.Atoi(limitparam)
	if err != nil {
		return 0, err
	}
	if page < 0 {
		return 0, errors.New("limit must be an positive integer")
	}
	if page == 0 {
		return 1, nil
	}
	return page, nil
}
