package controllers

import (
	"encoding/json"
	"net/http"
)

func bodyParser(r *http.Request, body interface{}) error {
	return json.NewDecoder(r.Body).Decode(&body)
}

func writeJSON(w http.ResponseWriter, data interface{}) {
	bytes, _ := json.MarshalIndent(data, "", "  ")

	w.Header().Set("Content-Type", "Application/json")
	w.Write(bytes)
}

type response struct {
	Error bool        `json:"error"`
	Data  interface{} `json:"data"`
}
