package controllers

import (
	"context"
	"net/http"

	pb "github.com/doston/iman/api_gateway/genproto/fetch_post_service"
	client "github.com/doston/iman/api_gateway/grpc_client"
)

// fetchPostRequest ...
type fetchPostRequest struct {
	FetchPostRequest string `json:"fetch_post_request"`
}

// FetchPosts ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Success 200 {object} response
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/fetch-posts/ [POST]
func FetchPosts(w http.ResponseWriter, r *http.Request) {

	_, err := client.FetchPostsService().FetchPosts(
		context.Background(), &pb.FetchPostsRequest{
			NumberOfPages: 55,
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while fetching posts") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, response{
		Error: false,
		Data:  "Success",
	})
}

// FetchPosts ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Success 200 {object} response
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/fetching-process/ [POST]
func IsEndedFetchingProcess(w http.ResponseWriter, r *http.Request) {

	_, err := client.FetchPostsService().FetchPosts(
		context.Background(), &pb.FetchPostsRequest{
			NumberOfPages: 55,
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while fetching posts") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, response{
		Error: false,
		Data:  "Success",
	})
}
