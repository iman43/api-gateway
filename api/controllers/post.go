package controllers

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	pb "github.com/doston/iman/api_gateway/genproto/post_service"
	client "github.com/doston/iman/api_gateway/grpc_client"
)

// getPostWithId ...
type getPostWithId struct {
	PostID int64  `json:"post_id"`
	UserID int64  `json:"user_id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

// CreatePost ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Param CreatePost body models.UpdatePostModel true "create_post_model"
// @Success 200 {object} response
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/create-post/ [POST]
func CreatePost(w http.ResponseWriter, r *http.Request) {
	var (
		body pb.Post
	)

	err := bodyParser(r, &body)

	post, err := client.PostService().CreatePost(
		context.Background(), &pb.CreatePostRequest{
			// Id:     body.Id,
			// UserId: body.UserId,
			// Title:  body.Title,
			// Body:   body.Body,
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while updating the post") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, response{
		Error: false,
		Data:  post,
	})
}

// GetPostWithId ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Param post_id path string true "Post ID"
// @Success 200 {object} GetPostWithId
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/get-post/{post_id}/ [GET]
func GetPostWithId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	postID := params["post_id"]
	postIdi, _ := strconv.Atoi(string(postID))

	res, err := client.PostService().GetPostWithId(
		context.Background(), &pb.GetPostWithIDRequest{
			Id: int64(postIdi),
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while getting post") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, getPostWithId{
		PostID: res.Id,
		UserID: res.UserId,
		Title:  res.Title,
		Body:   res.Body,
	})
}

// GetPosts ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Param limit query int false "limit"
// @Param page query int false "page"
// @Success 200 {object} response
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/list-posts/ [GET]
func GetPosts(w http.ResponseWriter, r *http.Request) {
	// http://localhost:8000/api/v1/post/list-posts/?limit=4&page=1
	page, err := ParsePageQueryParam(r)
	if handleBadRequestErrWithMessage(w, err, "error while parsing page") != nil {
		return
	}

	limit, err := ParseLimitQueryParam(r)
	if handleBadRequestErrWithMessage(w, err, "error while parsing limit") != nil {
		return
	}

	posts, err := client.PostService().GetPosts(
		context.Background(), &pb.GetPostsRequest{
			Page:  int32(page),
			Limit: int32(limit),
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while getting post") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, response{
		Error: false,
		Data:  posts,
	})
}

// UpdatePost ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Param UpdatePost body models.UpdatePostModel true "update post"
// @Success 200 {object} response
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/update-post/ [PUT]
func UpdatePost(w http.ResponseWriter, r *http.Request) {
	var (
		body pb.Post
	)

	err := bodyParser(r, &body)

	postID, err := client.PostService().UpdatePost(
		context.Background(), &pb.UpdatePostRequest{
			Id:     body.Id,
			UserId: body.UserId,
			Title:  body.Title,
			Body:   body.Body,
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while updating the post") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, response{
		Error: false,
		Data:  postID.Id,
	},
	)
}

// DeletePost ...
// @Description
// @Tags post
// @Accept json
// @Produce json
// @Param post_id path string true "post id"
// @Success 200 {object} response
// @Failure 400 {object} response
// @Failure 500 {object} response
// @Router /v1/post/delete-post/{post_id}/ [DELETE]
func DeletePost(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	postID := params["post_id"]
	postIdi, _ := strconv.Atoi(postID)

	_, err := client.PostService().DeletePost(
		context.Background(), &pb.DeletePostRequest{
			Id: int64(postIdi),
		},
	)

	if handleGrpcErrWithMessage(w, err, "Error while deleting the post") != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSON(w, response{
		Error: false,
		Data:  int64(postIdi),
	},
	)
}
