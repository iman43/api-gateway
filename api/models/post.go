package models

//SuccessMessage return boolean about success or not
type SuccessMessage struct {
	Success bool `json:"success"`
}

// UpdateRecipeModel ...
type UpdatePostModel struct {
	ID     int64  `json:"id"`
	UserID int64  `json:"user_id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}