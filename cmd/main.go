package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/doston/iman/api_gateway/configs"
	"go.uber.org/zap"

	_ "github.com/golang-migrate/migrate/v4/database"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/joho/godotenv/autoload" // load .env file automatically

	"github.com/doston/iman/api_gateway/routes"
	// _ "github.com/doston/iman/api_gateway/api/docs" //init swagger docs

	// "github.com/doston/iman/api/routes"
	"github.com/gorilla/mux"

	_ "github.com/lib/pq"
)

func main() {

	// Config
	conf := configs.Config()
	if err := conf.Validate(); err != nil {
		panic(err)
	}

	// HTTP
	root := mux.NewRouter()
	// API
	root.HandleFunc("/api/", HealthCheck).Methods("GET")

	routes.SwaggerRoute(root)
	routes.PostRoutes(root)
	routes.FetchRoutes(root)
	// routes.FetchPostsRoute(root)
	errChan := make(chan error, 1)
	osSignals := make(chan os.Signal, 1)

	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	httpServer := http.Server{
		Addr:    conf.HTTPPort,
		Handler: root,
	}
	// http server
	go func() {
		// logger.Info("Starting serving ", zap.Any("on port:", httpServer.Addr))
		errChan <- httpServer.ListenAndServe()
	}()

	// Blocking main and waiting for shutdown.
	select {
	case err := <-errChan:
		fmt.Println("error: ", zap.Error(err))

	case <-osSignals:
		fmt.Println("main : recieved os signal, shutting down")
		_ = httpServer.Shutdown(context.Background())
		return
	}
}

// HealthCheck godoc
// @Summary Show the status of server.
// @Description get the status of server.
// @Tags root
// @Accept */*
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router / [get]
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	res := map[string]interface{}{
		"data": "Server is up and running",
	}

	w.Header().Set("Content-Type", "application/json")

	bytes, _ := json.Marshal(res)
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)

	return
}
