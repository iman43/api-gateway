package grpcclient

import (
	"fmt"
	"sync"

	"github.com/doston/iman/api_gateway/configs"
	post "github.com/doston/iman/api_gateway/genproto/post_service"
	fetch "github.com/doston/iman/api_gateway/genproto/fetch_post_service"
	"google.golang.org/grpc"
)

var cfg = configs.Config()
var (
	onceUPostService   sync.Once
	onceFetchPostService sync.Once

	instanceFetchPostService fetch.FetchPostServiceClient
	instancePostService     post.PostServiceClient

)

// PostService ...
func PostService() post.PostServiceClient {
	onceUPostService.Do(func() {
		connUser, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("post service dial host: %s port:%d err: %s",
				cfg.PostServiceHost, cfg.PostServicePort, err))
		}

		instancePostService = post.NewPostServiceClient(connUser)
	})

	return instancePostService
}

// FetchPostsService ...
func FetchPostsService() fetch.FetchPostServiceClient {
	onceFetchPostService.Do(func() {
		connRecipe, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.FetchServiceHost, cfg.FetchServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("fetch posts service dial host: %s port: %d err: %s",
				cfg.FetchServiceHost, cfg.FetchServicePort, err))
		}

		instanceFetchPostService = fetch.NewFetchPostServiceClient(connRecipe)
	})

	return instanceFetchPostService
}
